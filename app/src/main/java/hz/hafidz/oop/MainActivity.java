package hz.hafidz.oop;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btnDroneBagus, btnDroneJelek, btnBagusTerbang, btnJelekTerbang, btnBagusKetinggian, btnJelekKetinggian,
            btnBagusKetinggianMin, btnJelekKetinggianMin, btnBagusEnergi, btnJelekEnergi,
            btnBagusEnergiMin, btnJelekEnergiMin, btnBagusMatikanMesin, btnJelekMatikanMesin;

    private TextView txtDroneJelek, txtDroneBagus, txtBagusEnergi, txtJelekEnergi;
    private DroneBagus droneBagus;
    private DroneJelek droneJelek;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initButton();
        initTextView();
        initDrone();
        setTxtDroneBagus();
        setTxtDroneJelek();
        setTxtDroneBagus();
        setTxtDroneJelek();
        onCLick();
    }

    private void setTxtDroneBagus() {
        txtDroneBagus.setText(droneBagus.getKetinggian() + "");
    }

    private void setTxtDroneJelek() {
        txtDroneJelek.setText(droneJelek.getKetinggian() + "");
    }

    private void setTxtBagusEnergi() {
        txtBagusEnergi.setText(droneBagus.getEnergi() + "");
    }

    private void setTxtJelekEnergi() {
        txtJelekEnergi.setText(droneJelek.getEnergi() + "");
    }

    private void initButton() {
        btnDroneBagus = findViewById(R.id.btnDroneBagus);
        btnDroneJelek = findViewById(R.id.btnDroneJelek);
        btnBagusTerbang = findViewById(R.id.btnBagusTerbang);
        btnJelekTerbang = findViewById(R.id.btnJelekTerbang);
        btnBagusKetinggian = findViewById(R.id.btnBagusKetinggian);
        btnJelekKetinggian = findViewById(R.id.btnJelekKetinggian);
        btnBagusKetinggianMin = findViewById(R.id.btnBagusKetinggianMin);
        btnJelekKetinggianMin = findViewById(R.id.btnJelekKetinggianMin);
        btnBagusEnergi = findViewById(R.id.btnBagusEnergi);
        btnJelekEnergi = findViewById(R.id.btnJelekEnergi);
        btnBagusEnergiMin = findViewById(R.id.btnBagusEnergiMin);
        btnJelekEnergiMin = findViewById(R.id.btnJelekEnergiMin);
        btnBagusMatikanMesin = findViewById(R.id.btnBagusMatikanMesin);
        btnJelekMatikanMesin = findViewById(R.id.btnJelekMatikanMesin);
    }

    private void initTextView() {
        txtDroneBagus = findViewById(R.id.txtBagusKetinggian);
        txtDroneJelek = findViewById(R.id.txtJelekKetinggian);
        txtBagusEnergi = findViewById(R.id.txtBagusEnergi);
        txtJelekEnergi = findViewById(R.id.txtJelekEnergi);
    }

    private void initDrone() {
        droneBagus = new DroneBagus(0, 0, 400, false);
        droneJelek = new DroneJelek(0, 0, 400, false);

    }

    private void onCLick() {
        btnDroneBagus.setOnClickListener(v -> {
            Toast.makeText(this, droneBagus.otomatisMendarat(), Toast.LENGTH_SHORT).show();
        });

        btnDroneJelek.setOnClickListener(v -> {
            Toast.makeText(this, droneJelek.otomatisMendarat(), Toast.LENGTH_SHORT).show();
        });

        btnBagusTerbang.setOnClickListener(v -> {
            Toast.makeText(this, droneBagus.terbang(), Toast.LENGTH_SHORT).show();
            setTxtDroneBagus();
        });

        btnJelekTerbang.setOnClickListener(v -> {
            Toast.makeText(this, droneJelek.terbang(), Toast.LENGTH_SHORT).show();
            setTxtDroneJelek();
        });

        btnBagusKetinggian.setOnClickListener(v -> {
            int ketinggian = droneBagus.getKetinggian() + 1;
            droneBagus.setKetinggian(ketinggian);
            setTxtDroneBagus();
        });

        btnJelekKetinggian.setOnClickListener(v -> {
            int ketinggian = droneJelek.getKetinggian() + 1;
            droneJelek.setKetinggian(ketinggian);
            setTxtDroneJelek();
        });

        btnBagusKetinggianMin.setOnClickListener(v -> {
            int ketinggian = droneBagus.getKetinggian() - 1;
            droneBagus.setKetinggian(ketinggian);
            setTxtDroneBagus();
        });

        btnJelekKetinggianMin.setOnClickListener(v -> {
            int ketinggian = droneJelek.getKetinggian() - 1;
            droneJelek.setKetinggian(ketinggian);
            setTxtDroneJelek();
        });

        btnBagusEnergi.setOnClickListener(v -> {
            int energi = droneBagus.getEnergi() + 1;
            droneBagus.setEnergi(energi);
            setTxtBagusEnergi();
        });

        btnJelekEnergi.setOnClickListener(v -> {
            int energi = droneJelek.getEnergi() + 1;
            droneJelek.setEnergi(energi);
            setTxtJelekEnergi();
        });

        btnBagusEnergiMin.setOnClickListener(v -> {
            int energi = droneBagus.getEnergi() - 1;
            droneBagus.setEnergi(energi);
            setTxtBagusEnergi();
        });

        btnJelekEnergiMin.setOnClickListener(v -> {
            int energi = droneJelek.getEnergi() - 1;
            droneJelek.setEnergi(energi);
            setTxtJelekEnergi();
        });

        btnBagusMatikanMesin.setOnClickListener(v -> {
            Toast.makeText(this, droneBagus.matikanMesin(), Toast.LENGTH_SHORT).show();
        });

        btnJelekMatikanMesin.setOnClickListener(v -> {
            Toast.makeText(this, droneJelek.matikanMesin(), Toast.LENGTH_SHORT).show();
        });
    }
}
