package hz.hafidz.oop;

public class DroneBagus extends Drone {


    public DroneBagus(int ketinggian, int kecepatan, int energi, boolean isTerbang) {
        super(ketinggian, kecepatan, energi, isTerbang);
    }

    String otomatisMendarat() {
        setKetinggian(0);
        setTerbang(false);
        return "Kembali Ketangan";
    }
}
