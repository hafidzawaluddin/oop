package hz.hafidz.oop;

public class DroneJelek extends Drone {

    public DroneJelek(int ketinggian, int kecepatan, int energi, boolean isTerbang) {
        super(ketinggian, kecepatan, energi, isTerbang);
    }

    String otomatisMendarat(){
        setEnergi(0);
        setKecepatan(0);
        setKetinggian(0);
        setTerbang(false);
        return "Diambil Tetangga";
    }
}
