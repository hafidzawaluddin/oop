package hz.hafidz.oop;

public class Drone {

    private boolean isTerbang;
    private int ketinggian;
    private int kecepatan;
    private int energi;


    public int getKetinggian() {
        return ketinggian;
    }

    public void setKetinggian(int ketinggian) {
        this.ketinggian = ketinggian;
    }

    public int getKecepatan() {
        return kecepatan;
    }

    public void setKecepatan(int kecepatan) {
        this.kecepatan = kecepatan;
    }

    public int getEnergi() {
        return energi;
    }

    public void setEnergi(int energi) {
        this.energi = energi;
    }

    public boolean isTerbang() {
        return isTerbang;
    }

    public void setTerbang(boolean terbang) {
        isTerbang = terbang;
    }

    public Drone(int ketinggian, int kecepatan, int energi, boolean isTerbang) {
        this.ketinggian = ketinggian;
        this.kecepatan = kecepatan;
        this.energi = energi;
        this.isTerbang = isTerbang;
    }

    String terbang() {
        setKetinggian(10);
        setKecepatan(5);
        if (isTerbang()) {
            return "Udah Terbang";
        } else {
            setTerbang(true);
            return "otw Terbang";
        }
    }

    String matikanMesin() {
        setKetinggian(0);
        setKecepatan(0);
        setEnergi(0);
        setTerbang(false);
        return "mati mesin";
    }
}
